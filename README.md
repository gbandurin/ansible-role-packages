# Ansible Role Packages

This role installs packages, using package manager suitable for host systems. 

## Pass me list

Packages to install are defined by "pkg_list" variable, which is a list of a dictionaries. Pass this variable to role, filling it with packages to install or to delete. Example:

```
pkg_list:
  - name: atop
  - name: htop
    state: present
  - name: apache
    state: absent
```

## Usage

- Go to your Ansible project directory, or create new one.
- Make "roles" subfolder and cd into it. 
- Clone this repository.
- [Optional] go to vars subfolder and edit variables if needed.
- Go to root folder of project, and create playbook, including or importing this role.

## Example playbook

```
---
- hosts: webservers
  tasks:
    - name: Import package install role
      import_role:
        name: ansible-role-packages
      vars:
        pkg_list: 
          - name: nginx
  ...
```

## Compatibility

Supported package manager on host machines:
  - apt
  - yum
  - dnf
This suits Debian, Ubuntu, RHEL, CentOS 6, 7, 8.

## License

MIT

You can use this configuration without any restrictions.
This file comes with no warranty and no responsibilities for any consequences caused by this configuration, or misconfiguration of this configuration.

## Author Information

George Bandurin ([gbandurin@yandex.ru](mailto:gbandurin@yandex.ru)


